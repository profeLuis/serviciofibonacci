﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica
{
    public partial class Form1 : Form
    {
        ServicioWebLocal.fibonacciSoapClient webService = new ServicioWebLocal.fibonacciSoapClient();
        public Form1()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Antes
            label1.Text = "EJECUTANDO PROCESO";
            
            //Manejador de Tarea.
            Task<int> tareaWs = webService.fibonacciNumberAsync(Convert.ToInt16(textBox1.Text));

            tareaWs.ContinueWith(x => {
                this.label1.Text = x.Result.ToString();
            },TaskScheduler.FromCurrentSynchronizationContext());
        }
        private void updateTextLabel1(string mensaje)
        {
            label1.Text = mensaje;
        }
    }
}
